##
##  OSSP platform - Unix Platform Identification
##  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of OSSP platform, a Unix platform identification
##  program which can be found at http://www.ossp.org/pkg/tool/platform/.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2.0 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this library; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  platform.pod: the manual page (language: Plain Old Document)
##

=pod

=head1 NAME

B<OSSP platform> - Unix Platform Identification

=head1 SYNOPSIS

B<platform>
[B<-F>|B<--format> I<FORMAT>]
[B<-S>|B<--sep> I<STRING>]
[B<-C>|B<--conc> I<STRING>]
[B<-L>|B<--lower>]
[B<-U>|B<--upper>]
[B<-v>|B<--verbose>]
[B<-c>|B<--concise>]
[B<-n>|B<--newline>]
[B<-d>|B<--debug>]

B<platform>
[B<-t>|B<--type> I<TYPE>]
[B<-n>|B<--newline>]
[B<-d>|B<--debug>]

B<platform>
[B<-V>|B<--version>]

B<platform>
[B<-h>|B<--help>]

=head1 DESCRIPTION

B<OSSP platform> is a flexible Unix platform identification program.
It distinguishes a platform according to its I<hardware architecture>
and I<operating system>. For both there is a I<class>, I<product> and
I<technology> identification. For each of those six identifications,
there is a I<verbose>, I<regular> and I<concise> version.

This leads to eighteen (2x3x3) available identification strings for each
platform, from which usually 2 are chosen in a particular situation.
This is done by assembling the platform identification string using a
I<FORMAT> string containing one or more identification constructs of the
forms "C<%[xx]>" (verbose), "C<%{xx}>" (regular) and "C<%E<lt>xxE<gt>>"
(concise).


=head1 OPTIONS

The following command line options are available.

=over 4

=item B<-F>, B<--format> I<FORMAT>

This option controls the output formatting of this program. It is a
plain-text string with the "C<%>I<xx>" constructs which expand to the
various platform information strings. "C<%{>I<xx>C<}>" is the canonical
regular version of the information. "C<%[>I<xx>C<]>" is the verbose
version of the information. "C<%E<lt>>I<xx>C<E<gt>>" is the concise
version of the information. In total, the following constructs
are available for expansion:

 %[ac]    verbose hardware architecture class
 %{ac}    regular hardware architecture class
 %<ac>    concise hardware architecture class

 %[ap]    verbose hardware architecture product
 %{ap}    regular hardware architecture product
 %<ap>    concise hardware architecture product

 %[at]    verbose hardware architecture technology
 %{at}    regular hardware architecture technology
 %<at>    concise hardware architecture technology

 %[sc]    verbose operating system class
 %{sc}    regular operating system class
 %<sc>    concise operating system class

 %[sp]    verbose operating system product
 %{sp}    regular operating system product
 %<sp>    concise operating system product

 %[st]    verbose operating system technology
 %{st}    regular operating system technology
 %<st>    concise operating system technology

The default I<FORMAT> string is "C<%{sp} (%{ap})>", providing the
regular operating system and hardware architecture product information.

=item B<-S>, B<--sep> I<STRING>

This option sets the word I<separation> string for the platform
information strings. By default it is "C< >" (whitespace). It is
especially used for separating the operating system name and
the operating system version.

=item B<-C>, B<--conc> I<STRING>

This option sets the word I<concatenation> string for the platform
information strings. By default it is "C</>". It is especially used to
concatenate multiple parts in operating system name and version parts.

=item B<-L>, B<--lower>

This options enforces conversion of the output to all I<lower> case.

=item B<-U>, B<--upper>

This options enforces conversion of the output to all I<upper> case.

=item B<-v>, B<--verbose>

This option enforces verbose versions of all expansion constructs
in I<FORMAT> string of option B<-F>. It is equal to specifying all
expansion constructs as "C<%[>I<xx>C<]>".

=item B<-c>, B<--concise>

This option enforces concise versions of all expansion constructs
in I<FORMAT> string of option B<-F>. It is equal to specifying all
expansion constructs as "C<%E<lt>>I<xx>C<E<gt>>".

=item B<-n>, B<--no-newline>

This option omits the usual trailing newline character in the output.

=item B<-t>, B<--type> I<TYPE>

This option is a meta option which internally sets options B<-F>, B<-S>,
B<-C>, B<-L>, B<-U>, B<-v> or B<-c> according to I<TYPE>. It can be used
to easily specify various commonly known outputs. The following I<TYPE>s
are available:

=over 4

=item B<binary>

Binary Package Id (OpenPKG RPM).
This is equal to "C<-F '%<ap>-%<sp>' -L -S '' -C '+'>"
and results in outputs like "C<ix86-freebsd4.9>" and "C<ix86-debian3.0>".

=item B<build>

Build-Time Checking (OpenPKG RPM).
This is equal to "C<-F '%<at>-%<st>' -L -S '' -C '+'>"
and results in outputs like "C<i686-freebsd4.9>" and "C<i586-linux2.4>".

=item B<gnu>

GNU F<config.guess> Style Id.
This is similar to B<build> and is equal to "C<-F '"%<at>-unknown-%<st>' -L -S '' -C '+'>"
and results in outputs like "C<i686-unknown-freebsd4.9>" and "C<i586-unknown-linux2.4>".

=item B<web>

HTTP Server Header Id.
This is equal to "C<-F '"%<sp>-%<ac>' -S '/' -C '+'>"
and results in outputs like "C<FreeBSD/4.9-iX86>" and "C<Debian/3.0-iX86>".

=item B<summary>

Human Readable Verbose Summary Information. This is equal to "C<-F
'Class: %[sc] (%[ac])\nProduct: %[sp] (%[ap])\nTechnology: %[st]
(%[at])' -S ' ' -C '/'>" and results in outputs like:

 Class:      4.4BSD (iX86)
 Product:    FreeBSD 4.9-RC (iX86)
 Technology: FreeBSD 4.9-RC (i686)

and

 Class:      LSB (iX86)
 Product:    Debian GNU/Linux 3.0 (iX86)
 Technology: GNU/Linux 2.2/2.4 (i686)

=item B<all-in-one>

All-In-One Full-Table Information. This just outputs really
all 2x2x3 identification strings as a table.

=back

=item B<-d>, B<--debug>

This option enables some internal debugging messages.

=item B<-V>, B<--version>

This option outputs the version information of B<OSSP platform> only.

=item B<-h>, B<--help>

This option outputs the usage information of B<OSSP platform> only.

=back

=head1 EXAMPLES

The following real-life use cases are known:

=over 4

=item B<OpenPKG> build-time decisions

 $ platform -c -L -S "" -C "+" -F "%at-%st"
 $ platform -c -L -S "" -C "+" -F "%ac-%sc"

=item B<OpenPKG> binary RPM packages

 $ platform -c -L -S "" -C "+" -F "%ap-%sp"

=item F<README> files

 $ platform -v -F "%sp (%ap)"
 $ platform -v -F "%sc (%ac)"

=item Debugging

 $ platform --type=all-in-one

=back

=head1 SUPPORT

B<OSSP platform> currently knows the following particular Unix platforms
in detail: FreeBSD, NetBSD, OpenBSD, Linux, Sun Solaris, SCO UnixWare,
QNX Neutrino, SGI IRIX, HP HP-UX, HP Tru64, IBM AIX and Apple MacOS X
Darwin.

All other Unix platforms are recognized through generic uname(1)
information and so usually can be identified sufficiently, although the
identification might be not as precise as possible.

=head1 SEE ALSO

http://www.ossp.org/pkg/tool/platform/.

uname(3), GNU F<config.guess>.

=head1 HISTORY

B<OSSP platform> was implemented in September 2003 by I<Ralf S.
Engelschall> for use in the B<OSSP> and B<OpenPKG> projects. It was
prompted by the need in B<OpenPKG> to have both product (for RPM
filenames) and technology (for build-time decisions) identifiers for the
Unix platforms, OpenPKG packages are maintained for. It was inspired by
the B<GNU> F<config.guess> and the old B<GNU shtool> F<guessos> command.

The major difference to B<GNU> F<config.guess> is that B<OSSP platform>
does not use a I<vendor> identification (cannot be determined most of
the time and is not used at all in all projects I've ever seen) and
is a lot more flexible (class, product and technology identifications
combined with verbose, regular and concise outputs). The drawback of
B<OSSP platform> is that it (still) knows less particular platforms,
although the generic platform identification is sufficient enough most
of the time.

=head1 AUTHOR

 Ralf S. Engelschall
 rse@engelschall.com
 www.engelschall.com

=cut

