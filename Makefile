
all:
	@echo "==== verbose ===="; \
	sh platform.sh -v; \
	echo "==== normal ===="; \
	sh platform.sh; \
	echo "==== concise ===="; \
	sh platform.sh -c; \
	echo "==== various ===="; \
	echo . | awk '{ printf("rpm-file:  "); }'; sh platform.sh -L -F "%ap-%sp" -S "" -C "+"; \
	echo . | awk '{ printf("rpm-check: "); }'; sh platform.sh -L -F "%at-%st" -S "" -C "+"

